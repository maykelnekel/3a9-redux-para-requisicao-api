import "./App.css";
import Digimons from "./components/digimons";
import Search from "./components/search";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Search />
        <Digimons />
      </header>
    </div>
  );
}

export default App;
