import { useState } from "react";
import { useDispatch } from "react-redux";
import { addDigimonThunks } from "../../store/modules/digimons/thunks";

const Search = () => {
  const [input, setInput] = useState("");
  const [error, setError] = useState(false);

  const dispath = useDispatch();

  const handleSearch = () => {
    setError(false);
    dispath(addDigimonThunks(input, setError));
    setInput("");
  };

  return (
    <div>
      <div>
        <h2>Procure pelo seu Digimon!</h2>
      </div>
      {error && <span>Digimon não encontrado!</span>}
      <div>
        <input
          value={input}
          onChange={(event) => setInput(event.target.value)}
          placeholder="Procure seu Digimon"
        />
        <button onClick={handleSearch}>Pesquisar</button>
      </div>
    </div>
  );
};

export default Search;
