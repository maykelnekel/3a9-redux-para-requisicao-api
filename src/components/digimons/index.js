import { useSelector } from "react-redux";

const Digimons = () => {
  const digimons = useSelector((state) => state.digimons);
  console.log(digimons);
  return (
    <div>
      <ul>
        <li>
          {digimons.map((digimon, index) => (
            <li key={index}>{digimon}</li>
          ))}
        </li>
      </ul>
    </div>
  );
};

export default Digimons;
