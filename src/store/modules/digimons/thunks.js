import axios from "axios";
import { addDigimon } from "./actions";

export const addDigimonThunks = (digimon, setError) => (dispatch) => {
  //   console.log(setError);
  axios
    .get(`https://digimon-api.vercel.app/api/digimon/name/${digimon}`)
    .then((res) => dispatch(addDigimon(res.data[0].name)))
    .catch((error) => setError(true));
};
